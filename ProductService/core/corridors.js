const { Pool } = require('pg');
const pool = new Pool();
const axios = require('axios');
const {Corridors} = require('../model/corritor')
const {Product} = require('../model/product')

var corritorsObject = []
exports.getFavoritesCorridors = (token) => {
    return pool.connect()
    .then(client => {
        return getProductsChoiceForUser(token)
        .then(products=> {
            let stringOrderFavorites = filterCorridorsFromProductsChoices(products)
            
            const queryOrderFavorites = "SELECT * FROM corridors JOIN (VALUES " + stringOrderFavorites +" ) as x(id, ordering) ON corridors.id = x.id ORDER BY x.ordering"
            const queryOrderNormal = "SELECT * FROM corridors WHERE corridors.id NOT IN( SELECT favorites.id FROM corridors as favorites JOIN (VALUES " + stringOrderFavorites +" ) as x(id, ordering) ON favorites.id = x.id ) "
            return client.query(queryOrderFavorites)
                .then(dataFavorites => {
                    return client.query(queryOrderNormal)
                    .then(extraData => {
                        for(var i = 0; i < dataFavorites.rows.length; i++) {
                            let corridor = new Corridors(dataFavorites.rows[i])
                            corritorsObject.push(corridor);
                            dataFavorites.rows.splice(i, 1);
                            i--; 
                        }
                        for(var i = 0; i < extraData.rows.length; i++) {
                            let corridor = new Corridors(extraData.rows[i])
                            corritorsObject.push(corridor);
                            extraData.rows.splice(i, 1);
                            i--; 
                        }

                        return addProductsToCorridors(token)
                        .then(response => {
                            return response
                        }).catch(e => {
                            return Promise.reject(err);
                        });
                        
                    }).catch(err => {
                        return Promise.reject(err);
                    })
                })
                .catch(err => {
                    console.log(err)
                    return Promise.reject(err);
                }); 
        }).catch(e => {
            return Promise.reject(e);
        })
    }).catch(e => {
        return Promise.reject(e);
    })
}

function getProductsChoiceForUser(token){
    return axios.get(process.env.USER_MONGO_SERVER + "api/v1/products", {
        headers: {
            'token': token
        }
    })
    .then(response => {
       return response.data
    }, response => {
        return response;
    }).then(response => {
        if(response && response.success){
            return response.products;
        }else{
            return Promise.reject();
        }
    })
    .catch(e => {
        return Promise.reject(e);
    });
}

function filterCorridorsFromProductsChoices(products){
    let corridorsVotes = [];
    for(var i = 0; i < products.length; i++){
        let corridorId = products[i].corridorId
        let userChoice = products[i].userChoice
        if(corridorsVotes[corridorId]){
            corridorsVotes[corridorId].votes = corridorsVotes[corridorId].votes+userChoice
        }else{
            corridorsVotes[corridorId] = ({
                corridorId: corridorId,
                votes: userChoice ? userChoice : 0
            })
        }
    }
    
    corridorsVotes = corridorsVotes.sort(compare)
    let stringToFilterCorridors= "";
    let orderCorriers = 1
    corridorsVotes.forEach((item,index)=> {
        if(!stringToFilterCorridors){
            stringToFilterCorridors = stringToFilterCorridors + "(" + item.corridorId + "," +orderCorriers+")"
        }else{
            stringToFilterCorridors = stringToFilterCorridors + ",(" + item.corridorId + "," +orderCorriers+")"
        }
        orderCorriers++;
    })
    return stringToFilterCorridors;
}

function compare(a, b) {
    // Use toUpperCase() to ignore character casing
    const value1 = a.votes;
    const value2 = b.votes;
  
    let comparison = 0;
    if (value1 < value2) {
      comparison = 1;
    } else if (value1 > value2) {
      comparison = -1;
    }
    return comparison;
  }

function addProductsToCorridors(token){
    return new Promise(function (resolve, reject) {
        for(let i =0; i < corritorsObject.length; i++){
            getProductsOfCorridor(corritorsObject[i].id,token)
                .then(products => {
                    if(i + 1 == corritorsObject.length){
                        corritorsObject[i].products = products;
                        resolve(corritorsObject);
                    }else{
                        corritorsObject[i].products = products;
                    }
                }).catch(e => {
                    return Promise.reject(e);
                })
        }
        
    })

    
}

function getProductsOfCorridor(corridorId,token){
    const page= 1
    return axios.get(process.env.PRODUCTS_MONGO_SERVER + "api/v1/products?page="+page+"&corridorId="+corridorId, {
        headers: {
            'token': token
        }
    })
    .then(response => {
       return response.data
    }, response => {
        return response;
    }).then(response => {
        if(response && response.success){
            return response.products;
        }else{
            return Promise.reject();
        }
    })
    .catch(e => {
        return Promise.reject(e);
    });
}
  