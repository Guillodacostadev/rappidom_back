var corridors = require('./corridors')

exports.assignRoutes = function (app) {
  app.get('/api/v1/corridors', corridors.getCorridors);
  app.get('/api/v1/corridors/favorites', corridors.getFavorites);
}
