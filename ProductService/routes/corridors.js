const express = require('express');
const router = express.Router();
const { Pool } = require('pg');
const pool = new Pool();
const SuccessResponse = require('./../utilities/responses').success;
const ErrorResponse = require('./../utilities/responses').error;
const errorCodes = require('./../utilities/errorCodes').errors;
const corridorsCore = require('../core/corridors')

exports.getCorridors = function(req, res, next){
  try {
    pool.connect()
    .then(client => {
        return client.query('SELECT * FROM corridors')
        .then(data => {
            var response = new SuccessResponse(1);
            response.corridors = data.rows;
            res.status(200).send(response);
        })
        .catch(err => {
            handleError(errorCodes.serverError, err);
        }); 
    });
  } catch (error) {
      handleError(errorCodes.serverError, res);
  }
}

exports.getFavorites = function(req, res, next){
    let token = req.header('token');
    if(token){
        corridorsCore.getFavoritesCorridors(token)
        .then(corridors => {
            var response = new SuccessResponse(1);
            response.corridors = corridors;
            res.status(200).send(response);
        }).catch(e=> {
            handleError(errorCodes.serverError, res);
        })
    }else{
        handleError(errorCodes.badRequest, res);
    }
}

function handleError(error, res) {
  var response = new ErrorResponse(error);
  res.status(error.code).send(response);
}