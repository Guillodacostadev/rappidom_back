const mongoose = require('mongoose');

var Product = new mongoose.Schema({
    product_id:{
        type: Number
    },
    name:{
        type: String,
        trim: true,
        minlength: 1
    },
    description:{
        type: String
    },
    image:{
        type: String
    },
    price: {
        type: Number
    }
})

module.exports = {Product}