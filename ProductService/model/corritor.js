const mongoose = require('mongoose');
const {Product} = require('./product')

var Corridors = mongoose.model('Corridors',{
    id:{
        type: Number,
        required: true
    },
    parent_id:{
        type: Number
    },
    name:{
        type: String,
        required: true,
        trim: true,
        minlength: 1
    },
    products: {
        type: [Product]
    },
    icon: {
        type: String,
    },description: {
        type: String
    },
    icon_grid: {
        type: String
    },
    icon_big: {
        type:String
    },
    image_big: {
        type:String
    },
    image_small: {
        type:String
    },
    store_id:{
        type: Number
    }
})

module.exports = {Corridors}