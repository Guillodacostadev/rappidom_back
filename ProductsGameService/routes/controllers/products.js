const lodash = require('lodash');
const {Product} = require('../../db/models/products')
var productCore = require('../../core/products')
const constants = require('../../constants/constants')
var SuccessResponse = require('../../utilities/responses').success;
var ErrorResponse = require('../../utilities/responses').error;
var errorCodes = require('../../utilities/errorCodes').errors;

exports.getProducts = function(req,res,next){
    let page = req.query.page;
    let corridorId = req.query.corridorId;
    
    productCore.getListProducts(page,corridorId).then((products) => {
        var response = new SuccessResponse(products.length);
        response.products = products;
        res.status(200).send(response);
    },(e) => {
        res.status(400).send(e);
    })
}

exports.getProduct = function(req,res,next){
    let productId = req.params.productId;
    productCore.getProduct(productId).then((product) => {
        let response = new SuccessResponse(product.length);
        response.product = product;
        res.status(200).send(response);
    },(e) => {
        handleError(errorCodes.serverError, res);
    })
}

exports.postLikeProduct = function(req,res,next){
    let token = req.header('token');
    if(token){
        let productId = req.params.productId;
        productCore.savePlayedProduct(productId,constants.productSelectableOption.Like,token)
        .then((product) => {
            let response = new SuccessResponse(product.length);
            response.product = product;
            res.status(200).send(response)
        }),(e) => {
            handleError(errorCodes.serverError, res);
        }
    }else{
        handleError(errorCodes.badRequest, res);
    }
}

exports.postNoLikeProduct = function(req,res,next){
    let token = req.header('token');
    if(token){
        let productId = req.params.productId;
        productCore.savePlayedProduct(productId,constants.productSelectableOption.NoLike,token)
        .then((product) => {
            let response = new SuccessResponse(product.length);
            response.product = product;
            res.status(200).send(response)
        }),(e) => {
            handleError(errorCodes.serverError, res);
        }
    }else{
        handleError(errorCodes.badRequest, res);
    }
}

exports.postSuperLikeProduct = function(req,res,next){
    let token = req.header('token');
    if(token){
        let productId = req.params.productId;
        productCore.savePlayedProduct(productId,constants.productSelectableOption.SuperLike,token)
        .then((product) => {
            let response = new SuccessResponse(product.length);
            response.product = product;
            res.status(200).send(response)
        }),(e) => {
            handleError(errorCodes.serverError, res);
        }
    }else{
        handleError(errorCodes.badRequest, res);
    }
}

exports.getRandomProducts = function(req,res,next){
    let token = req.header('token');
    if(token){
        productCore.getRandomListProducts().then((products) => {
            var response = new SuccessResponse(products.length);
            response.products = products;
            res.status(200).send(response);
        },(e) => {
            handleError(errorCodes.serverError, res);
        })
    }else{
        handleError(errorCodes.badRequest, res);
    }
}

function handleError(error, res) {
    var response = new ErrorResponse(error);
    res.status(error.code).send(response);
}

