var products = require('./controllers/products')

exports.assignRoutes = function (app) {
    app.get('/api/v1/products',products.getProducts)
    app.get('/api/v1/product/:productId',products.getProduct)
    app.post('/api/v1/product-like/:productId',products.postLikeProduct)
    app.post('/api/v1/product-no-like/:productId',products.postNoLikeProduct)
    app.post('/api/v1/product-super-like/:productId',products.postSuperLikeProduct)
    app.get('/api/v1/products-random',products.getRandomProducts)
}
