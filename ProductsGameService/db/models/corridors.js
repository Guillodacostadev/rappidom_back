const mongoose = require('mongoose');

var Corridors = new mongoose.Schema({
    id:{
        type: Number,
        required: true
    },
    parent_id:{
        type: Number
    },
    name:{
        type: String,
        required: true,
        trim: true,
        minlength: 1
    }
})

module.exports = {Corridors}