const mongoose = require('mongoose');
const {Corridors} = require('./corridors')

var Product = mongoose.model('products',{
    name:{
        type: String,
        required: true,
        trim: true
    },
    image:{
        type: String
    },
    description:{
        type: String
    },
    store_id:{
        type: Number,
        required: true
    },
    price:{
        type: Number,
        required: true
    },
    product_id:{
        type: Number,
        required: true
    },
    corridors: {
        type: [Corridors]
    }
});

module.exports = {Product}