require('./config/config')

var express = require('express');
var bodyParser = require('body-parser');

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

const routes = require('./routes/routes')

routes.assignRoutes(app)
module.exports = app;
