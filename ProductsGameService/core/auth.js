function getUser(token){
    return axios.get(process.env.USER_MONGO_SERVER + "api/v1/user", {
        headers: {
            'token': token
        }
    })
    .then(response => {
       return response
    }, response => {
        return response.response;
    }).then(response => {
        if(response && response.status === 200){
            return response.user
        }else{
            return Promise.reject();
        }
    })
    .catch(e => {
        return Promise.reject(e);
    });
}