const {mongoose} = require('../db/mongoose')
const {Product} = require('../db/models/products')
const axios = require('axios');


exports.getListProducts = (page,corridorId) =>{
    const pagesize = 10;
    return Product.find({
        "corridors.id": corridorId
    }).skip(pagesize*(page-1)).limit(pagesize).then((products) => {
        return products;
    }).catch((e) => {
        return Promise.reject(e);
    })
}

exports.getProduct = (product_id) =>{
    return getProduct(product_id).then((product) => {
        return product;
    }).catch((e) => {
        return Promise.reject(e);
    })
    
}

exports.getRandomListProducts = () =>{
    const pagesize = 10;
    return Product.aggregate([{$sample:{size: pagesize}}]).then((products) => {
        return products;
    }).catch((e) => {
        return Promise.reject(e);
    })
}

exports.savePlayedProduct = (product_id,selectableOption,token) =>{
    return getProduct(product_id)
    .then((product) => {
        return saveProductSelectable(product,selectableOption,token)
        .then((productSaved) =>{
            return productSaved
        }).catch((e) => {
            console.log("Respuesta incorrecta" + e)
            return Promise.reject();
        })
    }).catch((e) => {
        return Promise.reject(e);
    })
}


const getProduct = (product_id) => {
    return Product.findOne({product_id: product_id})
    .then((product) => {
        return product;
    }).catch((e) => {
        return Promise.reject(e);
    })
}

const saveProductSelectable = (product,selectableOption,token) => {
    let body = {
        name: product.name,
        productId: product.product_id,
        userChoice: selectableOption,
        corridorId: product.corridors[0].id,
        subCorridorId: product.corridors[1] ?  product.corridors[1].id :  '',
        userID: token
    };
    return axios.post(process.env.USER_MONGO_SERVER + "api/v1/products",body, {
        headers: {
            'token': token
        }
    })
    .then(response => {
       return response.data
    }, response => {
        return response;
    }).then(response => {
        console.log(response)
        if(response && response.success){
            return response;
        }else{
            return Promise.reject();
        }
    })
    .catch(e => {
        return Promise.reject(e);
    });
}

