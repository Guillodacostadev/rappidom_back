var stores = require('./stores')

exports.assignRoutes = function (app) {
  app.get('/api/v1/stores', stores.getStores);
}
