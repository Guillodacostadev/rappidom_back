var express = require('express');
var router = express.Router();
var { Pool } = require('pg');
const pool = new Pool();
var SuccessResponse = require('../utilities/responses').success;
var ErrorResponse = require('../utilities/responses').error;
var errorCodes = require('../utilities/errorCodes').errors;

exports.getStores = function(req, res, next){
  try {
    pool.connect()
    .then(client => {
        return client.query('SELECT * FROM stores')
        .then(data => {
            var response = new SuccessResponse(1);
            response.stores = data.rows;
            res.status(200).send(response);
        })
        .catch(err => {
            handleError(errorCodes.serverError, err);
        }); 
    });
  } catch (error) {
      console.log(error);
      handleError(errorCodes.serverError, res);
  }
}

function handleError(error, res) {
  var response = new ErrorResponse(error);
  res.status(error.code).send(response);
}