require('./config/config')

var express = require('express');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var path = require('path');
const routes = require('./routes/routes');
var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser())

routes.assignRoutes(app)
module.exports = app;