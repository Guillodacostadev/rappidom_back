const mongoose = require('mongoose');

var Product = mongoose.model('Products',{
    name:{
        type: String,
        required: true,
        trim: true
    },
    productId:{
        type: Number,
        required: true
    },
    userChoice: {
        type: Number,
        require: true
    },
    corridorId: {
        type: Number,
        require: true
    },
    subCorridorId: {
        type: Number,
        require: true
    },
    userID: {
        type: String,
        require: true
    }
});

module.exports = {Product}