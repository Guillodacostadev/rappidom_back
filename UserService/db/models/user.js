const mongoose = require('mongoose');
const validator = require('validator');

var User = mongoose.model('User',{
    token:{
        type: String,
        required: true,
        trim: true,
        minlength: 1,
    },
    facebookID:{
        type: String,
        required: true,
        trim: true,
        minlength: 1,
    },
    userName:{
        type: String,
        required: true,
        trim: true,
        minlength: 1
    },
    email: {
        type: String,
        required: true,
        trim: true,
        minlength: 1,
        validate: {
            validator: validator.isEmail,
            message: '{VALUE} is not a valid email'
        }
    },
    userAge:{
        type: String,
        require: false
    }
});

module.exports = {User}