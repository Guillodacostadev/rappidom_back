
const {mongoose} = require('../db/mongoose')
const {User} = require('../db/models/user')

exports.saveUser = (user) =>{
    return getUser(user.facebookID).then(user => {
        if(user){
            return user
        }else{
            return user.save().then(() => {
                return user;
            }).catch((e) => {
                return Promise.reject(e);
            })
        }
    })
}

exports.getUser = (token) => {
    return User.findOne({
        facebookID: token
    }).then((user) => {
        return user
    }).catch((e) => {
        return Promise.reject(e);
    })
}

function getUser(token){
    return User.findOne({
        facebookID: token
    }).then((user) => {
        return user
    }).catch((e) => {
        return Promise.reject(e);
    })
}