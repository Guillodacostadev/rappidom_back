const {mongoose} = require('../db/mongoose')
const {Product} = require('../db/models/product')
const userCore = require('./user')

exports.saveProduct = (product,userToken) =>{
    return userCore.getUser(userToken).then(user => {
        if(user){
            product.userID = user.facebookID

            product.save().then(() => {
                return product;
            }).catch((e) => {
                return Promise.reject(e);
            })
        }else{
            return Promise.reject();
        }
    }).catch(e=> {
        return Promise.reject(e);
    })
    
}


exports.getProducts = (userToken) => {
    return userCore.getUser(userToken).then(user => {
        if(user){
            return Product.find({
                userID: user.facebookID
            }).then(products => {
                return products;
            }).catch(e => {
                return Promise.reject(e);
            })
        }else{
            return Promise.reject();
        }
    }).catch(e => {
        return Promise.reject(e);
    })
}