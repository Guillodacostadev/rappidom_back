var users = require('./controllers/users')
var products = require('./controllers/products')

exports.assignRoutes = function (app) {
  app.post('/api/v1/login',users.loginUser)
  app.post('/api/v1/products',products.postProductSelected)
  app.get('/api/v1/products',products.getProductSelected)
}
