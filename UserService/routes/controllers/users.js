const lodash = require('lodash');
const {User} = require('../../db/models/user')
const userCore = require('../../core/user')
const SuccessResponse = require('../../utilities/responses').success;
const ErrorResponse = require('../../utilities/responses').error;
const errorCodes = require('../../utilities/errorCodes').errors;

exports.loginUser = function(req,res,next){
    let body = lodash.pick(req.body, [
        'facebookID',
        'token',
        'email',
        'userName',
        'userAge'
    ]);
    let user = new User(body);
    userCore.saveUser(user).then(() => {
        let response = new SuccessResponse(1);
        response.user = user;
        res.status(200).send(response);
    }).catch((e) => {
        handleError(errorCodes.serverError, res);
    })
}

exports.getUser = function(req,res,next){
    let token = req.header('token');
    if(token){
        userCore.getUser(token).then((user) => {
            let response = new SuccessResponse(1);
            response.user = user;
            res.status(200).send(response);
        }).catch((e) => {
            handleError(errorCodes.serverError, res);
        })
    }else{
        handleError(errorCodes.badRequest, res);
    }
}

function handleError(error, res) {
    let response = new ErrorResponse(error);
    res.status(error.code).send(response);
}