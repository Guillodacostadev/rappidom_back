const lodash = require('lodash');
const {Product} = require('../../db/models/product')
const productCore = require('../../core/product')
const SuccessResponse = require('../../utilities/responses').success;
const ErrorResponse = require('../../utilities/responses').error;
const errorCodes = require('../../utilities/errorCodes').errors;

exports.postProductSelected = function(req,res,next) {
    let token = req.header('token');
    if(token){
        let body = lodash.pick(req.body, [
            'name',
            'productId',
            'userChoice',
            'corridorId',
            'subCorridorId'
        ]);
        let product = new Product(body);
        productCore.saveProduct(product,token)
        .then(product => {
            let response = new SuccessResponse(1);
            response.product = product;
            res.status(200).send(response);
        }).catch(e => {
            handleError(errorCodes.serverError, res);
        })
    }else{
        handleError(errorCodes.badRequest, res);
    }
}

exports.getProductSelected = function(req,res,next){
    let token = req.header('token');
    if(token){
        productCore.getProducts(token)
        .then(products => {
            let response = new SuccessResponse(products.length);
            response.products = products;
            res.status(200).send(response);
        }).catch(e => {
            handleError(errorCodes.serverError, res);
        })
    }else{
        handleError(errorCodes.badRequest, res);
    }
}


function handleError(error, res) {
    let response = new ErrorResponse(error);
    res.status(error.code).send(response);
}