var subCorridors = require('./subCorridors')

exports.assignRoutes = function (app) {
  app.get('/api/v1/sub_corridors', subCorridors.getSubCorridors);
}
